var mainState = {
	gameState: null
	, cx: {}
	, create: function() {
		game.cleanup = [];
		// create physics groups
		game.projectiles = game.add.physicsGroup(); // all player projectiles
		game.projectiles.name = 'projectiles';
		game.enemies = game.add.physicsGroup();     // all enemies
		game.enemies.name = 'enemies';
		game.others = game.add.physicsGroup();      // everything else (smokes, explosions, etc.)
		game.others.name = 'others';

		// create player ship
		this.playerShip = new PlayerShip(game);
		this.playerShip.name = 'playerShip';
		game.add.existing(this.playerShip);

		// create spawner
		this.spawner = new Spawner(game, this.playerShip.x, this.playerShip.y, game.stages[PlayerData.stage]);
		this.spawner.onFinished.add(function () {
				if(this.gameState == 'play')
					this.gameState = 'waitClear';
				}, this);
		this.spawner.start();

		// start game
		this.gameState = 'play';
	}

	, update: function() {
		game.physics.arcade.overlap(game.projectiles, game.enemies, this.explodeShot);
		if(this.gameState == 'play' || this.gameState == 'waitClear')
			game.physics.arcade.overlap(this.playerShip, game.enemies, this.explodePlayer, null, this);
		this.cleanUp();

		switch(this.gameState) {
			case 'play':
				break;
			case 'waitClear':
				if(game.enemies.children.length == 0) {
					if(this.spawner.next()) {
						this.gameState = 'play';
					} else {
						this.gameState = 'win';
					}
				}
				break;
			case 'dead':
				PlayerData.submitScore = PlayerData.score;
				PlayerData.score = 0;

				// show dead message
				game.time.events.add(Phaser.Timer.SECOND * 4, function() {
					this.showDeadPopup();
				}, this);
				this.gameState = 'waitExit';
				break;
			case 'win':
				// show win message
				PlayerData.stage = Math.min(PlayerData.stage + 1, game.stages.length);

				game.time.events.add(Phaser.Timer.SECOND * 1, function() {
					this.openShop();
				}, this);
				this.gameState = 'waitExit';
				break;
			case 'waitExit':
				break;
		}
	}

	, pauseUpdate: function() {
		// XXX: hack to interact with buttons
		game.input.activePointer.dirty = true;
		game.input.update();

		if(this.cx.input) {
			this.cx.input.update();
		}
	}

	, render: function() {
//		for(var i = 0; i < game.projectiles.children.length; ++i) {
//			game.debug.body(game.projectiles.children[i]);
//		}
//		for(var i = 0; i < game.enemies.children.length; ++i) {
//			game.debug.body(game.enemies.children[i]);
//		}
	}

	, explodeShot: function(projectile, enemy) {
		enemy.playerHit = true;
		enemy.damage(projectile.power);
		enemy.playerHit = false;
		projectile.explode();
		projectile.kill();
	}

	, explodePlayer: function(player, enemy) {
		enemy.damage(50);
		player.explode();
		player.kill();
		this.gameState = 'dead';
	}

	, cleanUp: function() {
		while(game.cleanup.length > 0) {
			var dead = game.cleanup.pop();
			dead.destroy();
		}
	}

	, openShop: function() {
		game.paused = false;
		changeState('shop');
	}

	, showDeadPopup: function() {
		game.paused = true;
		this.cx.popup = game.add.sprite(game.world.centerX, game.world.centerY, 'losePopup');
		this.cx.popup.anchor.set(0.5, 0.5);

		var scoreLabel = this.game.make.text(0, -80, 'Score: ' + PlayerData.submitScore, {font: 'bold 18px arial', fill: '#DDDDDD', align: 'center'});
		scoreLabel.anchor.set(0.5, 0.5);
		this.cx.popup.addChild(scoreLabel);

		this.cx.input = game.make.inputField(0, -50);
		//this.cx.input.anchor.set(0.5, 0.5);
		this.cx.input.x -= this.cx.input.width / 2;
		this.cx.input.y -= this.cx.input.height / 2;
		this.cx.popup.addChild(this.cx.input);

		var submitButton = game.make.button(0, 0, 'menuButtons'
			, function() {
				this.cx.input.inputEnabled = false;
				submitButton.inputEnabled = false;
				submitScore(this.cx.input.value
					, PlayerData.submitScore
					, function() {
						console.log('yay! :D');
					}
					, function() {
						this.cx.input.inputEnabled = true;
						submitButton.inputEnabled = true;
						console.log('error :(');
					}
				);
			}, this, 13, 12, 14);
		submitButton.anchor.set(0.5, 0.5);
		this.cx.popup.addChild(submitButton);
		var shopButton = game.make.button(0, 60, 'menuButtons', this.openShop, this, 7, 6, 8);
		shopButton.anchor.set(0.5, 0.5);
		this.cx.popup.addChild(shopButton);
	}
};
