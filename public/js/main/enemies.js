var Enemy = function(game, typeName, x, y, key, variant) {
	var data = game.cache.getJSON('enemydata') || {};
	var config = data[typeName] || {};
	var variantConfig = (config.variants && config.variants[variant]) || {}

	var frame = variantConfig.frameName
			|| config.frameName
			|| variant;

	Phaser.Sprite.call(this, game, x, y, key, frame);
	this.anchor.setTo(0.5, 0.5);

	this.health = variantConfig.health !== undefined ? variantConfig.health
			: config.health !== undefined ? config.health
			: 10;
	this.points = variantConfig.points !== undefined ? variantConfig.points
			: config.points !== undefined ? config.points
			: 1;
	this.coins = variantConfig.coins !== undefined ? variantConfig.coins
			: config.coins !== undefined ? config.coins
			: 1;
	this.speed = variantConfig.speed !== undefined ? variantConfig.speed
			: config.speed !== undefined ? config.speed
			: 1;

	this.playerHit = false;
	this.events.onKilled.add(ExplodeEnemy, this);
}

Enemy.prototype = Object.create(Phaser.Sprite.prototype);
Enemy.prototype.constructor = Enemy;

/**
 * An enemy that slowly advances towards the player.
 */
Enemy.BoxEnemy = function(game, x, y) {
	Enemy.call(this, game, 'BoxEnemy', x, y, 'spritesheet');

	this.animations.add('box', AnimationConstants.boxEnemyFrameNames);
	this.animations.play('box', 10, true);
};

Enemy.BoxEnemy.prototype = Object.create(Enemy.prototype);
Enemy.BoxEnemy.prototype.constructor = Enemy.BoxEnemy;

Enemy.BoxEnemy.prototype.update = function() {
	game.physics.arcade.moveToXY(this, game.world.centerX, game.world.centerY, this.speed);
}

/**
 * A fast enemy that makes a spiral-like trajectory.
 */
Enemy.SlimEnemy = function(game, x, y) {
	Enemy.call(this, game, 'SlimEnemy', x, y, 'spritesheet');

	this.animations.add('anim', AnimationConstants.slimEnemyFrameNames);
	this.animations.play('anim', 10, true);

	this.destinations = [];
	var radii = [150, 130, 110, 90, 70];
	var current = new Phaser.Point(x, y);
	var center = new Phaser.Point(game.world.centerX, game.world.centerY);
	var distance = current.distance(center);
	for(var i = 0; i < radii.length; ++i) {
		if(distance > radii[i]) {
			// right triangle calculation
			// var _h = distance;
			// var _a = radii[i];
			// var _b = Math.sqrt(_h*_h - _a*_a);

			// calculate tangent lines
			var px = current.x;
			var py = current.y;
			var cx = center.x;
			var cy = center.y;
			var radius = radii[i];

			// find tangents
			var dx = cx - px;
			var dy = cy - py;
			var dd = Math.sqrt(dx * dx + dy * dy);
			var a = Math.asin(radius / dd);
			var b = Math.atan2(dy, dx);

			var t = b - a
			var ta = new Phaser.Point(cx + radius * Math.sin(t), cy + radius * -Math.cos(t));

			var t = b + a
			var tb = new Phaser.Point(cx + radius * -Math.sin(t), cy + radius * Math.cos(t));

			if(isClockwise(current, ta, center)) {
				this.destinations.push(ta);
				current = ta;
			} else {
				this.destinations.push(tb);
				current = tb;
			}
			distance = current.distance(center);
		}
	}
	this.destinations.push(center);
	this.destinations.reverse();
}

Enemy.SlimEnemy.prototype = Object.create(Enemy.prototype);
Enemy.SlimEnemy.prototype.constructor = Enemy.SlimEnemy;

Enemy.SlimEnemy.prototype.update = function() {
	var i = this.destinations.length - 1;
	game.physics.arcade.moveToXY(this, this.destinations[i].x, this.destinations[i].y, this.speed);
	this.rotation = game.physics.arcade.angleBetween(this, this.destinations[i]) - 1.57079633;
	if(i > 0 && new Phaser.Point(this.x, this.y).distance(this.destinations[i]) < 1) {
		this.destinations.pop();
	}
}

function ExplodeEnemy(gameObject) {
	if(gameObject.playerHit) {
		PlayerData.score += gameObject.points;
		PlayerData.money += gameObject.coins;
		var timer = gameObject.game.time.create();
		var p1 = new Phaser.Point(15, 0);
		var p2 = new Phaser.Point(15, 0);
		var p3 = new Phaser.Point(15, 0);
		p1.rotate(0, 0, gameObject.game.rnd.between(-Math.PI, Math.PI));
		p2.rotate(0, 0, gameObject.game.rnd.between(-Math.PI, Math.PI));
		p3.rotate(0, 0, gameObject.game.rnd.between(-Math.PI, Math.PI));
		var dummy = gameObject.game.others.create(gameObject.x, gameObject.y);
		timer.add(  0, Spawn.bigExplosion, dummy,    0,    0);
		timer.add(150, Spawn.bigExplosion, dummy, p1.x, p1.y);
		timer.add(300, Spawn.bigExplosion, dummy, p2.x, p2.y);
		timer.add(450, Spawn.bigExplosion, dummy, p3.x, p3.y);
		timer.start();
		dummy.lifespan = 1000;
		dummy.events.onKilled.add(queueCleanup, this);
	}
	queueCleanup(gameObject);
}

function isClockwise(current, next, center) {
	return ((current.x - center.x)*(next.y - center.y) - (current.y - center.y)*(next.x - center.x)) > 0;
}