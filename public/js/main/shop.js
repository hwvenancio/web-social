var shopState = {
	create: function() {
		var x = game.world.centerX;
		var y = game.world.centerY;

		// x
		// y
		// sprite
		// callback
		this.laserButton = game.add.button(x - 50, y, 'shopButtons', this.buyLaser, this, 4, 3, 5);
		this.laserButton.fixedToCamera = true;
		this.laserButton.anchor.set(0.5, 0.5);
		this.missileButton = game.add.button(x + 50, y, 'shopButtons', this.buyMissile, this, 1, 0, 2);
		this.missileButton.fixedToCamera = true;
		this.missileButton.anchor.set(0.5, 0.5);
		var backButton = game.add.button(x + 160, y + 100, 'menuButtons', this.gotoMenu, this, 10, 9, 11);
		backButton.fixedToCamera = true;
		backButton.anchor.set(0.5, 0.5);
		backButton.alignIn(game.world.bounds, Phaser.BOTTOM_RIGHT, -10, -10);
		var startButton = game.add.button(x + 20, y + 100, 'menuButtons', this.startGame, this, 1, 0, 2);
		startButton.fixedToCamera = true;
		startButton.anchor.set(0.5, 0.5);
		startButton.alignTo(backButton, Phaser.LEFT_CENTER, 10, 0);

		this.weaponPower = game.add.graphics(-20,19);
		this.updateWeaponPowerBar();
	}
	, render: function() {
		game.debug.text('Money = ' + PlayerData.money, 16, 20);
	}

	, buyMissile: function() {
		buyWeapon('MissileLauncher');
		this.updateWeaponPowerBar();
	}
	, buyLaser: function() {
		buyWeapon('Laser');
		this.updateWeaponPowerBar();
	}
	, startGame: function() {
		changeState('main');
	}
	, gotoMenu: function() {
		changeState('menu');
	}
	, updateWeaponPowerBar: function() {
		switch(PlayerData.weapon.type) {
		case 'MissileLauncher':
			this.missileButton.addChild(this.weaponPower);
			break;
		case 'Laser':
			this.laserButton.addChild(this.weaponPower);
			break;
		}
		var weapondata = game.cache.getJSON('weapondata');
		var currentWeapon = weapondata.find(findFilter('type', PlayerData.weapon.type));
		var currentLevel = currentWeapon.levels.find(findFilter('level', PlayerData.weapon.level));
		var fill = (currentLevel.level / currentWeapon.levels.length);
		this.weaponPower.clear();
		var c = Phaser.Color.toRGBA(
		    255
		    , 255
		    , 255*fill
		    , 0);
		this.weaponPower.lineStyle(5, c, 1);
		this.weaponPower.moveTo(0,0);
		this.weaponPower.lineTo(40 * fill, 0);
	}
}