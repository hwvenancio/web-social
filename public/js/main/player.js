var PlayerShip = function(game) {
	Phaser.Sprite.call(this, game, game.world.centerX, game.world.centerY, 'spritesheet');

	game.physics.arcade.enable(this);

	this.anchor.setTo(0.5, 0.5);
	this.animations.add('idle', AnimationConstants.firstShipFrameNames);
	this.animations.play('idle', 10, true);
	this.weapon = new Weapon[PlayerData.weapon.type](this, PlayerData.weapon.level);

	this.events.onKilled.add(this.playerKilled, this);
}

PlayerShip.prototype = Object.create(Phaser.Sprite.prototype);
PlayerShip.prototype.constructor = PlayerShip;

PlayerShip.prototype.update = function() {
	this.rotation = this.game.physics.arcade.angleToPointer(this) + 1.57079633;
	if(this.game.input.mousePointer.isDown || this.game.input.pointer1.isDown) {
		this.weapon.handleFire();
	}
}

PlayerShip.prototype.explode = function() {
	var timer = this.game.time.create();
	timer.add(  0, Spawn.bigExplosion, dummy, 0, 0);
	var dummy = this.game.others.create(this.x, this.y);
	for(var i = 150; i < 4000; i += 150) {
		var p = new Phaser.Point(15, 0);
		p.rotate(0, 0, this.game.rnd.between(-Math.PI, Math.PI));
		timer.add(i, Spawn.bigExplosion, dummy, p.x, p.y);
	}
	timer.start();
	dummy.lifespan = 5000;
	dummy.events.onKilled.add(queueCleanup, this);
}

PlayerShip.prototype.playerKilled = function() {
    // spawn explosions
    this.explode();

    // spawn fake ship
    var fakeShip = this.game.add.sprite(game.world.centerX, game.world.centerY, 'spritesheet');
    fakeShip.anchor.set(0.5, 0.5);
    fakeShip.rotation = this.rotation;

	fakeShip.tint = 0xFF0000;
	fakeShip.lifespan = 4000;
	fakeShip.events.onKilled.add(queueCleanup, this);
}