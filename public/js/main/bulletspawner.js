var BulletSpawner = function(game, parent, data, callback, callbackContext) {
    this.game = game;
    //this.parent = parent || null;
    //this.data = data || null;
    this.callback = callback;
    this.callbackContext = callbackContext;
    parent = parent || {};
    data = data || {};
    this.frameName = data.frameName || parent.frameName || 0;
    this.power = data.power || parent.power || 1;
    this.position = data.position || parent.position || 'center';
    this.direction = data.direction || parent.direction || 'forward';
    this.scatter = data.scatter || parent.scatter || 0;
    this.reloadTime = data.reloadTime || parent.reloadTime || 100;
    this.bulletTime = 0;
    switch(this.position) {
        case 'left':
            this.offsetX = -8;
            this.offsetY = 0;
            break;
        case 'right':
            this.offsetX = 8;
            this.offsetY = 0;
            break;
        case 'center':
        default:
            this.offsetX = 0;
            this.offsetY = 0;
            break;
    }
}

BulletSpawner.prototype.fireBullet = function() {
    if(this.game.time.now > this.bulletTime) {
        this.bulletTime = this.game.time.now + this.reloadTime;
        this.callback.call(this.callbackContext, this);
    }
}