Enemy.Asteroid = function(game, typeName, x, y, variant) {
    variant = variant || 0;

	Enemy.call(this, game, typeName, x, y, 'tyrian-newshd', variant);

	var data = game.cache.getJSON('enemydata') || {};
	var config = data[typeName] || {};
	var variantConfig = (config.variants && config.variants[variant]) || {}

	var angle = variantConfig.angle !== undefined ? variantConfig.angle
			: config.angle !== undefined ? config.angle
			: 0;

	game.physics.arcade.enable(this);
	this.body.velocity.set(this.speed*2, 0);

    if(this.inWorld) {
        this.checkWorldBounds = true;
        this.events.onOutOfBounds.add(killGameObject, this);
    } else {
        this.events.onEnterBounds.add(function() {
    		this.checkWorldBounds = true;
	    	this.events.onOutOfBounds.add(killGameObject, this);
        }, this);
    }
}
Enemy.Asteroid.prototype = Object.create(Enemy.prototype);
Enemy.Asteroid.prototype.constructor = Enemy.Asteroid;

Enemy.Asteroid.prototype.update = function() {
    var b = new Phaser.Rectangle(0, 0, game.stage.width, game.stage.height);
    if(b.contains(this.x, this.y)) {
	    this.body.velocity.setMagnitude(this.speed);
	} else {
	    this.body.velocity.setMagnitude(this.speed*2);
    }
}

/**
 * Tiny asteroid, low health and points.
 */
Enemy.TinyAsteroid = function(game, x, y, variant) {
	Enemy.Asteroid.call(this, game, 'TinyAsteroid', x, y, variant);
}
Enemy.TinyAsteroid.prototype = Object.create(Enemy.Asteroid.prototype);
Enemy.TinyAsteroid.prototype.constructor = Enemy.TinyAsteroid;

/**
 * Small asteroid, a little more health and points.
 */
Enemy.SmallAsteroid = function(game, x, y, variant) {
	Enemy.Asteroid.call(this, game, 'SmallAsteroid', x, y, variant);
}
Enemy.SmallAsteroid.prototype = Object.create(Enemy.Asteroid.prototype);
Enemy.SmallAsteroid.prototype.constructor = Enemy.SmallAsteroid;

/**
 * Big asteroid, medium health and points.
 */
Enemy.BigAsteroid = function(game, x, y, variant) {
	Enemy.Asteroid.call(this, game, 'BigAsteroid', x, y, variant);
}
Enemy.BigAsteroid.prototype = Object.create(Enemy.Asteroid.prototype);
Enemy.BigAsteroid.prototype.constructor = Enemy.BigAsteroid;

/**
 * Huge asteroid, tough and more points.
 */
Enemy.HugeAsteroid = function(game, x, y) {
	Enemy.Asteroid.call(this, game, 'HugeAsteroid', x, y, 'asteroid_huge1_pt2');

	var chunk1 = game.make.sprite(0, -28, 'tyrian-newshd', 'asteroid_huge1_pt1');
	var chunk2 = game.make.sprite(0, +28, 'tyrian-newshd', 'asteroid_huge1_pt3');
	chunk1.anchor.set(0.5, 0.5);
	chunk2.anchor.set(0.5, 0.5);
	this.addChild(chunk1);
	this.addChild(chunk2);

	this.body.setSize(72, 84, 0, -28);
}
Enemy.HugeAsteroid.prototype = Object.create(Enemy.Asteroid.prototype);
Enemy.HugeAsteroid.prototype.constructor = Enemy.HugeAsteroid;