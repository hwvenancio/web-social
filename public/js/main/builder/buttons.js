var ButtonGenerator = function(game, name, buttonWidth, buttonHeight) {
    this.game = game;
    this.name = name;
    this.buttonWidth = buttonWidth;
    this.buttonHeight = buttonHeight;
    // normal, over, down
    this.highColor = [0x6666FF, 0x8888FF, 0x333399];
    this.lowColor = [0x993399, 0xBB55BB, 0x662266];
    this.textColor = ['#DDDDDD', 'white', 'black'];
    //
    this.buttonCount = 0;
    this.buttonType = [];
    this.buttonContent = [];

}

ButtonGenerator.prototype.addText = function(text) {
    this.buttonType.push('text');
    this.buttonContent.push(text);
    this.buttonCount++;
    return this;
}

ButtonGenerator.prototype.addImage = function(image) {
    this.buttonType.push('image');
    this.buttonContent.push(image);
    this.buttonCount++;
    return this;
}

ButtonGenerator.prototype.generate = function() {
    function generateBackground(bmd, index) {
		var w = this.buttonWidth;
		var y = this.buttonHeight * index;
		for(var i = 0; i < this.buttonHeight; ++i) {
			var c1 = Phaser.Color.interpolateColor(this.highColor[0], this.lowColor[0], this.buttonHeight, i);
			var c2 = Phaser.Color.interpolateColor(this.highColor[1], this.lowColor[1], this.buttonHeight, i);
			var c3 = Phaser.Color.interpolateColor(this.highColor[2], this.lowColor[2], this.buttonHeight, i);
			bmd.rect(0  , y, w, 1, Phaser.Color.getWebRGB(c1));
			bmd.rect(w  , y, w, 1, Phaser.Color.getWebRGB(c2));
			bmd.rect(w*2, y, w, 1, Phaser.Color.getWebRGB(c3));
			++y;
		}
    }
    function generateText(bmd, index) {
        var text = this.buttonContent[index];
        var offsetX = this.buttonWidth / 2;
		var offsetY = this.buttonHeight / 2 + this.buttonHeight * index;
		var xs = [offsetX, offsetX + this.buttonWidth, offsetX + this.buttonWidth * 2];
        var y = offsetY;

		for(var i = 0; i < 3; ++i) {
			var x = xs[i];
			var c = this.textColor[i];
			var textSprite = this.game.make.text(0, 0, text, {font: 'bold 18px arial', fill: c, align: 'center'});
			textSprite.anchor.set(0.5); //	Center align, center anchor
			bmd.draw(textSprite, x, y);
		}
    }
    function generateImage(bmd, index) {
		var image = this.buttonContent[index];
		var offsetX = this.buttonWidth / 2;
		var offsetY = this.buttonHeight / 2 + this.buttonHeight * index;
		var xs = [offsetX, offsetX + this.buttonWidth, offsetX + this.buttonWidth * 2];
		var y = offsetY;

		for(var i = 0; i < 3; ++i) {
			var x = xs[i];
    		bmd.draw(image, x, y);
    	}
    }
    var bmd = this.game.make.bitmapData(this.buttonWidth * 3, this.buttonHeight * this.buttonCount);
    for(var i = 0; i < this.buttonCount; ++i) {
        generateBackground.call(this, bmd, i);
        switch(this.buttonType[i]) {
            case 'text':
                generateText.call(this, bmd, i);
                break;
            case 'image':
                generateImage.call(this, bmd, i);
                break;
        }
    }
	this.game.load.spritesheet(this.name, bmd.canvas.toDataURL(), this.buttonWidth, this.buttonHeight);
}