var PopupGenerator = function(game, name, popupWidth, popupHeight) {
	this.game = game;
	this.name = name;
	this.popupWidth = popupWidth;
	this.popupHeight = popupHeight;
	this.highColor = 0x666666;
	this.lowColor = 0x333333;
}

PopupGenerator.prototype.generate = function() {
	function generateBackground(bmd) {
	    var w = this.popupWidth;
        var y = 0;
	    for(var i = 0; i < this.popupHeight; ++i) {
		    var c = Phaser.Color.interpolateColor(this.highColor, this.lowColor, this.popupHeight, i);
		    bmd.rect(0, y, w, 1, Phaser.Color.getWebRGB(c));
		    ++y;
		}
	}
	var bmd = this.game.make.bitmapData(this.popupWidth, this.popupHeight);

	generateBackground.call(this, bmd);

	this.game.load.image(this.name, bmd.canvas.toDataURL());
}