var AnimationConstants = {
	firstShipFrameNames: Phaser.Animation.generateFrameNames('firstShip', 1, 4)
	, boxEnemyFrameNames: Phaser.Animation.generateFrameNames('box', 1, 4)
	, slimEnemyFrameNames: Phaser.Animation.generateFrameNames('slim', 1, 4)
	, smokeFrameNames: Phaser.Animation.generateFrameNames('whiteSmoke', 1, 8)
	, laserExplosionFrameNames: Phaser.Animation.generateFrameNames('explosion', 1, 13)
	, bigExplosionLeftFrameNames: Phaser.Animation.generateFrameNames('bigExplosion_l', 1, 17)
	, bigExplosionRightFrameNames: Phaser.Animation.generateFrameNames('bigExplosion_r', 1, 17)
}

var PlayerData = {
	stage: 0
	, score: 0
	, money: 0
	, weapon: {type: "Laser", level: 1}
}

function disableEmitter(gameObject) {
	gameObject.emitter.on = false;
}
function killGameObject(gameObject) {
	gameObject.kill();
}
function queueCleanup(gameObject) {
	game.cleanup.push(gameObject);
}
function changeState(stateName) {
	game.state.start(stateName);
}
function submitScore(name, score, success, failure) {
	var data = {"name":name, "score":score};
	data = JSON.stringify(data);
	var request = new XMLHttpRequest();
	request.open('POST', '/score', true);
	request.setRequestHeader('Content-Type', 'application/json');
	request.onload = function() {
		if (request.status >= 200 && request.status < 400) {
		    if(success) success(request.responseText);
		} else {
		    if(failure) failure();
		}
	};
	request.onerror = function() {
		if(failure) failure();
	};
	request.send(data);
}
function queryScores(page, size, success, failure, context) {
    page = page || 0;
    size = size || 10;
    context = context || null;
    var query = 'page=' + page + '&size=' + size;
	var request = new XMLHttpRequest();
    request.open("GET", '/score?'+query, true);
    request.onload = function() {
		if (request.status >= 200 && request.status < 400) {
		    if(success) success.call(context, JSON.parse(request.responseText));
		} else {
		    if(failure) failure.call(context);
		}
	};
	request.onerror = function() {
		if(failure) failure.call(context);
	};
    request.send(null);
}
function buyWeapon(type) {
	var level = 1;
	if(PlayerData.weapon.type == type) {
		level = PlayerData.weapon.level + 1;
	}
	var weapondata = game.cache.getJSON('weapondata');
	var currentWeapon = weapondata.find(findFilter('type', PlayerData.weapon.type));
	var currentLevel = currentWeapon.levels.find(findFilter('level', PlayerData.weapon.level));
	var nextWeapon = weapondata.find(findFilter('type', type));
	if(nextWeapon == null)
		return;
	var nextLevel = nextWeapon.levels.find(findFilter('level', level));
	if(nextLevel == null)
		return;
	var cost = nextLevel.cost - currentLevel.cost;
	if(cost < PlayerData.money) {
		PlayerData.money -= cost;
		PlayerData.weapon.type = nextWeapon.type;
		PlayerData.weapon.level = nextLevel.level;
	}
}

function findFilter(prop, value) {
	return function(item) { return item[prop] == value; };
}

/**
 *  Invoke with Spawn.BigExplosion.call(sprite);
 */
var Spawn = {
	smallExplosion: function() {
		var sprite = this.game.others.create(this.x, this.y, 'tyrian-newsh6');
		sprite.anchor.setTo(0.5, 0.5);
		sprite.body.velocity.x = this.body.velocity.x / 4.0;
		sprite.body.velocity.y = this.body.velocity.y / 4.0;
		sprite.animations.add('anim', AnimationConstants.laserExplosionFrameNames);
		sprite.animations.play('anim', 30, false, true);
		sprite.events.onKilled.add(queueCleanup, this);
	}
	, bigExplosion: function(offsetX, offsetY) {
		offsetX = offsetX || 0;
		offsetY = offsetY || 0;
		var spriteL = this.game.others.create(this.x + offsetX, this.y + offsetY, 'tyrian-newsh6');
		spriteL.anchor.setTo(1.0, 0.5);
		spriteL.animations.add('anim', AnimationConstants.bigExplosionLeftFrameNames);
		spriteL.animations.play('anim', 30, false, true);
		spriteL.events.onKilled.add(queueCleanup, this);
		var spriteR = this.game.others.create(this.x + offsetX, this.y + offsetY, 'tyrian-newsh6');
		spriteR.anchor.setTo(0.0, 0.5);
		spriteR.animations.add('anim', AnimationConstants.bigExplosionRightFrameNames);
		spriteR.animations.play('anim', 30, false, true);
		spriteR.events.onKilled.add(queueCleanup, this);
	}
};