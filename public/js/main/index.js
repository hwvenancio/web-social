var game;

function body_onload() {
	game = new Phaser.Game(500, 340, Phaser.AUTO, 'gameDiv');
	game.state.add('load', loadState);
	game.state.add('menu', menuState);
	game.state.add('shop', shopState);
	game.state.add('leaderboard', leaderboardState);
	game.state.add('main', mainState);
	game.state.start('load');
}

