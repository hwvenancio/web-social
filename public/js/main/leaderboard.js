var leaderboardState = {
	page: 0
	, size: 10
	, pages: 0
	, cx: null
	, create: function() {
	    // reset
	    this.page = 0;
	    this.size = 10;
	    this.cx = {
            names: []
            , scores: []
            , first: null
            , prev: null
            , next: null
            , last: null
        };
	    // stage rectangle
	    var stageRect = new Phaser.Rectangle(0, 0, game.stage.width, game.stage.height);

	    var w = 300;
	    var h = 180;
	    var left = stageRect.width/2 - w/2;
	    var right = stageRect.width/2 + w/2;
	    var y = stageRect.height/2 - h/2 - 25;

        // scores rectangle
   		var scoreRect = new Phaser.Rectangle(left, y, w, h);
   		this.scoreRect = scoreRect;

        // generate labels
	    var fontSize = 14;
	    for(var i = 0; i < this.size; ++i) {
	        var even = (i % 2) == 0;
	        var color = even ? '#999999' : '#FFFFFF';
		    var nameLabel = game.add.text(left, y, '', {font: 'bold 14px arial', fill: color});
		    var scoreLabel = game.add.text(right, y, '', {font: 'bold 14px arial', fill: color});
		    nameLabel.anchor.set(0.0, 0.0);
		    scoreLabel.anchor.set(1.0, 0.0);
		    this.cx.names.push(nameLabel);
		    this.cx.scores.push(scoreLabel);
		    y += fontSize+4;
	    }

        // generate buttons
		var firstButton= game.add.button(0, 0, 'paginationButtons', this.gotoFirst, this, 1, 0, 2);
		var prevButton = game.add.button(0, 0, 'paginationButtons', this.gotoPrev, this, 4, 3, 5);
		var nextButton = game.add.button(0, 0, 'paginationButtons', this.gotoNext, this, 7, 6, 8);
		var lastButton = game.add.button(0, 0, 'paginationButtons', this.gotoLast, this, 10, 9, 11);
		var backButton = game.add.button(0, 0, 'menuButtons'      , this.gotoMenu, this, 10, 9, 11);

		firstButton.inputEnabled = false;
		prevButton.inputEnabled = false;
		nextButton.inputEnabled = false;
		lastButton.inputEnabled = false;
        // align right to scores
//		lastButton.alignTo(scoreRect, Phaser.BOTTOM_RIGHT, 0, 0);
//		nextButton.alignTo(lastButton, Phaser.LEFT_CENTER, 10, 0);
//		prevButton.alignTo(nextButton, Phaser.LEFT_CENTER, 10, 0);
//		firstButton.alignTo(prevButton, Phaser.LEFT_CENTER, 10, -10);
        // align left to scores
		firstButton.alignTo(scoreRect, Phaser.BOTTOM_LEFT, 0, 0);
		prevButton.alignTo(firstButton, Phaser.RIGHT_CENTER, 10, 0);
		nextButton.alignTo(prevButton, Phaser.RIGHT_CENTER, 10, 0);
		lastButton.alignTo(nextButton, Phaser.RIGHT_CENTER, 10, 0);

        // align to stage
		backButton.alignIn(stageRect, Phaser.BOTTOM_RIGHT, -10, -10);

		this.cx.first = firstButton;
		this.cx.prev = prevButton;
		this.cx.next = nextButton;
		this.cx.last = lastButton;

        this.updateScores();
	}
	, render: function() {
	    game.debug.rectangle(this.scoreRect, '#ffffff', false);
	}

	, gotoFirst: function() {
	    this.page = 0;
	    this.updateScores();
	}
	, gotoPrev: function() {
	    --this.page;
	    this.updateScores();
	}
	, gotoNext: function() {
	    ++this.page;
	    this.updateScores();
	}
	, gotoLast: function() {
	    this.page = this.pages - 1;
	    this.updateScores();
	}
	, gotoMenu: function() {
		changeState('menu');
	}
    , updateScores: function() {
	    this.disablePagination();
        queryScores(this.page, this.size, this.updateLabels, this.enablePagination, this);
    }
    , updateLabels: function(data) {
        this.page = data.page;
//        this.size = data.size;
        this.pages = data.pages;
        var pos = 1 + this.page * this.size;
        for(var i=0; i < this.cx.names.length; ++i) {
            var hasIndex = data.result && data.result.length > i;
		    this.cx.names[i].text = pos++ + '. ' + (hasIndex ? data.result[i].name : '');
		    this.cx.scores[i].text = hasIndex ? data.result[i].score : '';
        }
        this.enablePagination();
    }
    , disablePagination: function() {
        this.cx.first.inputEnabled = false;
        this.cx.prev.inputEnabled = false;
        this.cx.next.inputEnabled = false;
        this.cx.last.inputEnabled = false;
    }
    , enablePagination: function() {
        this.cx.first.inputEnabled = this.page > 0;
        this.cx.prev.inputEnabled = this.page > 0;
        this.cx.next.inputEnabled = this.page < this.pages - 1;
        this.cx.last.inputEnabled = this.page < this.pages - 1;
        var overFrame = [1, 4, 7, 10];
        var outFrame = [0, 3, 6, 9];
        var buttons = [this.cx.first, this.cx.prev, this.cx.next, this.cx.last];
        for(var i=0; i < buttons.length; ++i) {
            buttons[i].frame = buttons[i].inputEnabled && buttons[i].input.checkPointerOver(game.input.mousePointer) ? overFrame[i] : outFrame[i];
        }
    }
};