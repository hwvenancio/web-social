var Spawner = function(game, x, y, stageName) {
	this.game = game;
	this.x = x;
	this.y = y;
	this.data = game.cache.getJSON(stageName);

    this.waves = [];

    var keys = [];
    for(var key in this.data) {
        keys.push(key);
    }
    keys.reverse();
    for(var i = 0; i < keys.length; ++i) {
        var key = keys[i];
        var wave = new Spawner.Wave(this, this.data[key]);
        this.waves.push(wave);
    }

	this.onFinished = new Phaser.Signal();
};

Spawner.Wave = function(spawner, waveData) {
    this.spawner = spawner;
    this.timer = game.time.create();
	var max = 0;
	for(var i = 0; i < waveData.spawns.length; ++i) {
		var row = waveData.spawns[i];
		this.timer.add(row.delay, spawner.spawn, spawner, row);
		max = Math.max(max, row.delay);
	}
    this.timer.add(max+1, function() { spawner.onFinished.dispatch(); }, this);
}

Spawner.prototype.start = function() {
    return this.next();
}

Spawner.prototype.next = function() {
    if(this.waves.length == 0)
        return false;
    var wave = this.waves.pop();
    wave.timer.start();
    return true;
}

Spawner.prototype.spawn = function(data) {
	var enemyType = data.type;
	var x = data.x;
	var y = data.y;
	var variant = data.variant;
	var Constructor = Enemy[data.type];
	if(Constructor) {
	    var enemy = new Constructor(this.game, this.x + x, this.y + y, variant);
	    this.game.enemies.add(enemy);
	    if(data.angle !== undefined) {
	        enemy.body.velocity.rotate(0, 0, Phaser.Math.degToRad(data.angle));
	    }
	}
}