var menuState = {
	create: function() {
		var x = game.world.centerX;
		var y = game.world.centerY;

		// x
		// y
		// sprite
		// callback
		var startButton = game.add.button(x, y + 20, 'menuButtons', this.startGame, this, 1, 0, 2);
		startButton.anchor.set(0.5, 0.5);
		var shopButton = game.add.button(x, y + 80, 'menuButtons', this.openShop, this, 7, 6, 8);
		shopButton.anchor.set(0.5, 0.5);
		var leaderboardButton = game.add.button(x, y + 140, 'menuButtons', this.viewLeaderboard, this, 4, 3, 5);
		leaderboardButton.anchor.set(0.5, 0.5);
	}

	, startGame: function() {
		changeState('main');
	}
	, openShop: function() {
		changeState('shop');
	}
	, viewLeaderboard: function() {
		changeState('leaderboard');
	}
};
