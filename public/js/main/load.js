var loadState = {
	preload: function() {
		game.load.onFileComplete.add(this.generateButtons, this);
		game.load.onFileComplete.add(this.generatePopups, this);
		game.load.onFileComplete.add(this.fileComplete, this);
		loadSprites(game);
		loadData(game);
	}

	, create: function() {
		// install plugins
		game.add.plugin(Fabrique.Plugins.InputField);
		game.add.plugin(Phaser.Plugin.Debug);

		// start physics
		game.physics.startSystem(Phaser.Physics.ARCADE);

        // set world bounds
        game.world.setBounds(-50, -50, 600, 440);

		changeState('menu');
	}

	// fix background alpha, load atlas and overwrite cache key
	, alphaFixed: []
	, fileComplete: function(progress, cacheKey, success, totalLoaded, totalFiles) {
		if(this.alphaFixed.indexOf(cacheKey) != -1)
			return;
		console.log(cacheKey);
		switch(cacheKey) {
			case 'spritesheet' : {
				// firstShip spritesheet
				var bmd = fixBackgroundAlpha(game, 'spritesheet', 0xFFFFFFFF, 0xFFEFEFEF);
				game.load.atlas('spritesheet', bmd.canvas.toDataURL(), 'assets/spritesheet.json');
				break;
			}
			case 'tyrian-000000' : {
				// bullets and missiles
				bmd = fixBackgroundAlpha(game, 'tyrian-000000', 0xFFBFDCBF, 0xFFB6E2BD);
				game.load.atlas('tyrian-000000', bmd.canvas.toDataURL(), 'assets/tyrian.000000.json');
				break;
			}
			case 'tyrian-newsh6' : {
				// smokes and explosions spritesheet
				bmd = fixBackgroundAlpha(game, 'tyrian-newsh6', 0xFFBFDCBF, 0xFFB6E2BD);
				game.load.atlas('tyrian-newsh6', bmd.canvas.toDataURL(), 'assets/tyrian.newsh6.json');
				break;
			}
			case 'tyrian-newsh1' : {
				// shop weapons
				bmd = fixBackgroundAlpha(game, 'tyrian-newsh1', 0xFFBFDCBF, 0xFFB6E2BD);
				game.load.atlas('tyrian-newsh1', bmd.canvas.toDataURL(), 'assets/tyrian.newsh1.json');
				break;
			}
			case 'tyrian-newshd' : {
				// asteroids
				bmd = fixBackgroundAlpha(game, 'tyrian-newshd', 0xFFBFDCBF, 0xFFB6E2BD);
				game.load.atlas('tyrian-newshd', bmd.canvas.toDataURL(), 'assets/tyrian.newshd.json');
				break;
			}
			default:
				return;
		}
		this.alphaFixed.push(cacheKey);
	}
	, generateButtons: function(progress, cacheKey, success, totalLoaded, totalFiles) {
		if(cacheKey != 'tyrian-newsh1')
			return;
		if(this.alphaFixed.indexOf(cacheKey) == -1)
			return;

		game.load.onFileComplete.remove(this.generateButtons, this);

		var btnGen = new ButtonGenerator(game, 'menuButtons', 120, 50);
		btnGen.addText('Start');
		btnGen.addText('Leaderboard');
		btnGen.addText('Shop');
		btnGen.addText('Back');
		btnGen.addText('Submit');
		btnGen.generate();

		var image1 = game.make.sprite(0, 0, 'tyrian-newsh1', 'missile')
		image1.anchor.set(0.5, 0.5);
		var image2 = game.make.sprite(0, 0, 'tyrian-newsh1', 'laser');
		image2.anchor.set(0.5, 0.5);
		var btnGen = new ButtonGenerator(game, 'shopButtons', 50, 50);
		btnGen.addImage(image1);
		btnGen.addImage(image2);
		btnGen.generate();

        var btnGen = new ButtonGenerator(game, 'paginationButtons', 40, 40);
        btnGen.addText('<<');
        btnGen.addText('<');
        btnGen.addText('>');
        btnGen.addText('>>');
        btnGen.generate();
	}
	, generatePopups: function() {
	    game.load.onFileComplete.remove(this.generatePopups, this);

        var popupGen = new PopupGenerator(game, 'losePopup', 200, 200);
        popupGen.generate();
	}
};


function fixBackgroundAlpha(game, sheetname) {
	var image = game.cache.getImage(sheetname);
	var width = image.width;
	var height = image.height;
	var bmd = game.make.bitmapData(width, height);
	bmd.load(sheetname);
	for(var i = 2; i < arguments.length; ++i) {
		var color = arguments[i];
		var a = (color >> 24) & 0xFF;
		var r = (color >> 16) & 0xFF;
		var g = (color >>  8) & 0xFF;
		var b = (color      ) & 0xFF;
		bmd.replaceRGB(r, g, b, a
					,  0x00, 0x00, 0x00, 0x00);
	}
	return bmd;
}

function loadSprites(game) {
	// firstShip spritesheet
	game.load.image('spritesheet', 'assets/spritesheet.png');
	// bullets and missiles
	game.load.image('tyrian-000000', 'assets/tyrian/tyrian.shp.000000.png');
	// smokes and explosions spritesheet
	game.load.image('tyrian-newsh6', 'assets/tyrian/newsh6.shp.000000.png');
	// shop weapons
	game.load.image('tyrian-newsh1', 'assets/tyrian/newsh1.shp.000000.png');
	// asteroids
	game.load.image('tyrian-newshd', 'assets/tyrian/newshd.shp.000000.png');
}

function loadData(game) {
	// load weapon data
	game.load.json('weapondata', 'assets/data/weapons.json');
	// load enemies data
	game.load.json('enemydata', 'assets/data/enemies.json');
	// load stages
	game.stages = ['stage1', 'stage2', 'stage3', 'stage4', 'death_stage'];
	game.load.json('stage1', 'assets/data/stage1.json');
	game.load.json('stage2', 'assets/data/stage2.json');
	game.load.json('stage3', 'assets/data/stage3.json');
	game.load.json('stage4', 'assets/data/stage4.json');
	game.load.json('death_stage', 'assets/data/death_stage.json');
}