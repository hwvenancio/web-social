/**
 * Base Weapon class, stores a reference to game and weapon level
 */
var Weapon = function(game, weaponName, level) {
    this.game = game;
	this.level = level;
    this.bulletTime = 0;
    this.spawners = [];

    var data = game.cache.getJSON('weapondata')
        .find(function (weapon) { return weapon.type == weaponName; });
    var weaponLevel = data.levels
        .find(function (item) { return item.level == level});
    var spawns = weaponLevel.spawns || [{}];
    for(var i = 0; i < spawns.length; ++i) {
        this.spawners.push(new BulletSpawner(game, weaponLevel, spawns[i], this.fireBullet, this));
    }
}
Weapon.prototype.handleFire = function() {
    for(var i = 0; i < this.spawners.length; ++i) {
        this.spawners[i].fireBullet();
    }
}
Weapon.prototype.fireBullet = function(data) {
}

var Bullet = (function() {
    var explode = function() {
    }
    return function(sprite, power) {
        sprite.power = power;
        sprite.explode = explode;
    }
})();

var LaserBullet = (function() {
    var explode = function() {
        Spawn.smallExplosion.call(this);
    }
    return function(sprite, power) {
        Bullet(sprite, power);
        sprite.explode = explode;
    }
})();

var MissileBullet = (function() {
    var explode = function() {
        Spawn.bigExplosion.call(this);
    }

    return function(sprite, power) {
        Bullet(sprite, power);
        sprite.explode = explode;
    }
})();

Weapon.Laser = function(playerShip, level) {
    Weapon.call(this, playerShip.game, 'Laser', level);
    this.playerShip = playerShip;
}

// Laser extends Weapon
Weapon.Laser.prototype = Object.create(Weapon.prototype);
Weapon.Laser.prototype.constructor = Weapon.Laser;

Weapon.Laser.prototype.fireBullet = function(data) {
    var rotation = this.playerShip.rotation;
    // create laser group
    var laser;
    {
        laser = this.game.projectiles.create(this.playerShip.x, this.playerShip.y, 'tyrian-000000', data.frameName);
        laser.anchor.setTo(0.5, 0.5);
        laser.rotation = rotation;
        laser.body.velocity.x = 0;
        laser.body.velocity.y = -200;
        laser.body.velocity.rotate(0, 0, rotation);
        laser.checkWorldBounds = true;
        laser.events.onOutOfBounds.add(killGameObject, this);
        laser.events.onKilled.add(queueCleanup, this);
        LaserBullet(laser, data.power);
    }
}

Weapon.MissileLauncher = function (playerShip, level) {
    Weapon.call(this, playerShip.game, 'MissileLauncher', level);
    this.playerShip = playerShip;
}
// MissileLauncher extends Weapon
Weapon.MissileLauncher.prototype = Object.create(Weapon.prototype);
Weapon.MissileLauncher.prototype.constructor = Weapon.MissileLauncher;

Weapon.MissileLauncher.prototype.fireBullet = function(data) {
    var rotation = this.playerShip.rotation;
    // create missile group
    var missile;
    {
        var offset = new Phaser.Point(data.offsetX, offsetY);
        offset.rotate(0, 0, rotation);
        missile = this.game.projectiles.create(this.playerShip.x + offset.x, this.playerShip.y + offset.y, 'tyrian-000000', data.frameName);
        missile.anchor.setTo(0.5, 0.5);
        missile.rotation = rotation;
        missile.body.maxVelocity = 100;
        missile.body.velocity.x = this.game.rnd.between(-data.scatter, data.scatter);
        missile.body.velocity.y = 10;
        missile.body.velocity.rotate(0, 0, rotation);
        missile.body.acceleration.y = -100;
        missile.body.acceleration.rotate(0, 0, rotation);
        missile.checkWorldBounds = true;
        missile.events.onOutOfBounds.add(killGameObject);
        missile.events.onKilled.add(function() {
            // Transfer emitter to a 'phantom' body that will carry remaining
            // particles for a while, maintaining inertial movement. This results
            // in a more interesting effect than destroying everything
            var group = this.game.others.create(missile.x, missile.y);
            group.rotation = missile.rotation;
            group.body.velocity.x = missile.body.velocity.x;
            group.body.velocity.y = missile.body.velocity.y;
            group.lifespan = 1000;
            group.events.onKilled.add(queueCleanup, this);
            group.emitter = missile.emitter;
            group.addChild(missile.emitter);
            disableEmitter(group);
            queueCleanup(missile);
        }, this);
        MissileBullet(missile, data.power);
    }
    // create smoke particle emitter
    var emitter;
    {
        var offsetX = 0;
        var offsetY = 8;
        emitter = this.game.add.emitter(offsetX, offsetY, 10);
        // choose a random smoke particle sprite
        emitter.makeParticles('tyrian-newsh6', AnimationConstants.smokeFrameNames);
        emitter.setAlpha(0.3, 0.3);
        emitter.setXSpeed(0, 0);
        emitter.setYSpeed(0, 0);
        emitter.setRotation(0, 0);
        emitter.gravity = 150;
        emitter.start(false, 1000, 100);
        missile.emitter = emitter;
        missile.addChild(emitter);
    }
}
