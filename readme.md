# start mongodb server daemon
docker run --name mongo-instance -p 27017:27017 -d mongo
# connect to running instance as admin
docker exec -it mongo-instance mongo game
# create empty scores collection
db.createCollection("scores");
exit

# build image and run instance
docker build -t web-social .
docker run --link mongo-instance:mongo -p 3000:3000 -it --rm --name web-social-instance web-social

# verify docker machine ip (windows and mac only)
docker-machine ip default

# clean up instance and image
docker stop web-social-instance
docker rm web-social-instance
docker rmi web-social

# all in one:
sudo docker build -t web-social . \
&& sudo docker run --link mongo-instance:mongo -p 3000:3000 -it --rm --name web-social-instance web-social \
&& sudo docker rmi web-social

# additional info:
free mongodb:
https://www.mlab.com/databases/heroku_6d9zc3r6












