var http = require('http');
var express = require('express');
var bodyParser = require('body-parser');
var mongo = require('mongodb');

// configure web server
var app = express();
app.use(express.static('./public'));
app.set('view engine', 'ejs');
app.set('views', './views');

var urlencodedParser = bodyParser.urlencoded({ extended: false });
app.use(urlencodedParser);
app.use(bodyParser.json());

app.get('/', function (req, res) {
	var model = {};
	res.render('index', model);
});

app.get('/score', urlencodedParser, function (req, res) {
	var page = parseInt(req.query.page) || 0;
	var size = parseInt(req.query.size) || 10;

	var scores = db.collection("scores");

	var cursor = scores.find({}, {"_id":0}).sort({"score":-1});
	var pages;
	var result;

	cursor.count(function (err, count) {
		pages = Math.ceil(count / size);

        cursor.skip(page*size).limit(size);
		cursor.toArray(function(err, array) {
			result = array;
			res.json({
				"page": page
				, "size": size
				, "pages": pages
				, "result": result
			});
		});
	});
});

app.post('/score', urlencodedParser, function (req, res) {
	if (!req.body)
		return res.sendStatus(500)

	var data = req.body;
	if(data.name && data.score) {
		var scores = db.collection("scores");
		scores.insertOne({"name": data.name, "score" : data.score});
		var result = { success : true }
		res.json(result);
	} else {
		var result = { success : false }
		res.json(result);
	}
});

// evennode/heroku compatibility
var port = process.env.PORT || 3000;

// configure mongodb and start server
var DB_URI = process.env.MONGODB_URI;
var db;
mongo.MongoClient.connect(DB_URI, function(err, database) {
	if (err) return console.log(err)
	db = database;
	http.createServer(app)
		.listen(port, function() {
			console.log('Express Server is running');
		});
});